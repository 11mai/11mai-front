import Vue from 'vue'
import VueRouter from 'vue-router'
import Connexion from '../views/Connexion.vue'
import Home from '../views/Home.vue'
import CreateAccount from '../views/CreateAccount.vue'
import Profil from '@/views/Profil.vue'

Vue.use(VueRouter)

const routes = [
	{
		path: '/',
		name: 'Connexion',
		component: Connexion,
	},
	{
		path: '/accueil',
		name: 'Home',
		component: Home,
	},
	{
		path: '/creationCompte',
		name: 'CreateAccount',
		component: CreateAccount,
	},
	{
		path: '/profil',
		name: 'Profil',
		component: Profil,
		props: true,
	},
]

const router = new VueRouter({
	mode: 'history',
	base: process.env.BASE_URL,
	routes,
})

export default router
