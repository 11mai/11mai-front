module.exports = {
	css: {
		loaderOptions: {
			sass: {
				prependData: `@import "@/assets/scss/main.scss";`,
			},
		},
	},
	configureWebpack: {
		devtool: 'source-map'
	},
}
